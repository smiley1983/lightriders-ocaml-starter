OCaml starter package for Lightriders
=====================================

see starapple.riddles.io/competitions/light-riders for details of the game
--------------------------------------------------------------------------

The provided "main.ml" file is a minimal bot which makes random legal moves.

As seen in that example, your bot should provide a function which takes a game_state as its argument, and calls issue_order once.

You then provide this function as an argument to the Game.run_bot function.

Have a look at td.ml to see the definition of the game_state data type.

